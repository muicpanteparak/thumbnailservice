from logger import Logger
# from rabbitmq import Connector as WorkerConnector
import rabbitmq
import requests
import os
import json
from make_thumbnail import make_gif
from exception import ObjectNotFoundException, TicketCreationException, ObjectCreationException
import env
import hashlib
import shutil

LOG = Logger("app")

BUCKET_NAME = "videos"

HTTP_STATUS_OK = 200
HTTP_STATUS_BAD_REQUEST = 400
HTTP_STATUS_NOT_FOUND = 404
QUEUE_HOST = env.load_env("QUEUE_HOST")
QUEUE_PORT = env.load_env("QUEUE_PORT")
STORAGE_URL = env.load_env("STORAGE_URL")

def decoder(func):
    def wrapper_function(*args, **kwargs):
        _, __, ___, body = args
        arg = json.loads(body.decode('utf-8')),
        return func(*arg, *kwargs)
    return wrapper_function

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

@decoder
def consumer(body):
    try:
        LOG.info(msg="Consuming Job...")

        bucket_name, object_name, gif_name = body["bucket_name"], body["object_name"], body["gif_name"]
        LOG.debug(msg=f"Job: {bucket_name}/{object_name} -> {gif_name}")

        path_to_video, bucket_name, object_name = download_file("./video", bucket_name, object_name)

        GIF_PUT = os.path.join(os.path.abspath(path_to_video), bucket_name, object_name + ".gif")

        make_gif(os.path.join(path_to_video, bucket_name, object_name), GIF_PUT)
        upload_file(os.path.abspath(os.path.join(path_to_video, bucket_name, object_name + ".gif")), bucket_name,
                    object_name + ".gif")

        # os.rmdir(os.path.abspath(path_to_video))
        shutil.rmtree(os.path.abspath(path_to_video), ignore_errors=True)
        LOG.info(msg="Consumed Job")
    except Exception as e:
        LOG.info(str(e))


def download_file(path_to_video: str, bucket_name: str, object_name: str):
    LOG.debug(msg=f"Downloading object {bucket_name}/{object_name}")
    PATH = os.path.join(path_to_video, bucket_name, object_name)

    req = requests.get(f"{STORAGE_URL}/{bucket_name}/{object_name}", stream=True)

    if req.status_code != HTTP_STATUS_OK:
        LOG.debug(msg=f"File or Bucket Not Found, returned status: {req.status_code}")
        raise ObjectNotFoundException("Could Not Find Object")

    folder = os.path.join(path_to_video, bucket_name)

    if not os.path.exists(folder):
        LOG.info(msg=f"making folder at: {folder}")
        os.makedirs(folder)
    with open(PATH, 'wb') as file:
        LOG.debug(msg=f"Saving file at {path_to_video}")
        for chunk in req.iter_content(chunk_size=32 * 1024):
            if chunk:
                file.write(chunk)
        req.close()
    return path_to_video, bucket_name, object_name


def upload_file(abs_path_to_gif: str, bucket_name: str, object_name: str):

    with requests.Session() as s:
        try:
            s.post(f"{STORAGE_URL}/{bucket_name}?create")
        except:
            pass

    with requests.Session() as s:
        resp_create = s.post(f"{STORAGE_URL}/{bucket_name}/{object_name}?create")
        if resp_create.status_code != HTTP_STATUS_OK:
            LOG.info(f"Status Failed: {resp_create.status_code}, /{bucket_name}/{object_name}")
            raise TicketCreationException("Failed to Create Ticket or Ticket already exist")

    LOG.info(msg=f"MD5ing {abs_path_to_gif}")
    val = md5(abs_path_to_gif)
    with requests.Session() as ss:
        resp_put = ss.put(f"{STORAGE_URL}/{bucket_name}/{object_name}?partNumber=1",
                          data=open(abs_path_to_gif, "rb"),
                          headers={"Content-MD5": val})
        if resp_put.status_code != HTTP_STATUS_OK:
            LOG.info(f"Status Failed: {resp_put.status_code}")
            raise ObjectCreationException

    with requests.Session() as sss:
        resp_complete = sss.post(f"{STORAGE_URL}/{bucket_name}/{object_name}?complete")
        if resp_complete.status_code != HTTP_STATUS_OK:
            raise TicketCreationException("Failed to Complete Ticket")


def main():
    # rabbit_connector = WorkerConnector(host=QUEUE_HOST, port=int(QUEUE_PORT))
    rabbit_connector = rabbitmq.init()
    c = rabbit_connector.consume(consumer, "video")
    LOG.info(msg=f"Work Queue started at {QUEUE_HOST}:{QUEUE_PORT}")
    rabbit_connector.start_consuming(c)


if __name__ == '__main__':
    # upload_file("/Users/panteparak/Downloads/videoplayback.mp4", "abcd", "ababa121")
    try:
        main()
    except Exception as e:
        LOG.debug(msg=str(e))
    #
    # # print(md5("/Users/panteparak/Downloads/videoplayback.mp4"))
    # upload_file("/Users/panteparak/Downloads/videoplayback.mp4", "abcd","ababa121")
