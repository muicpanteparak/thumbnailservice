#!/usr/bin/env python3

from moviepy.editor import VideoFileClip
import sys
import os
from logger import Logger

LOG = Logger("make_thumbnail")


def make_gif(input_file, output_file):
    if os.path.isabs(input_file):
        path = input_file
    else:
        path = os.path.abspath(os.path.join(input_file))
    video = VideoFileClip(path)
    start_time = video.duration * 2/3
    gif = video.subclip(start_time, min(start_time + 10, video.duration)).resize((320, 240))
    LOG.info(msg="Writing Video")
    gif.write_gif(os.path.abspath(os.path.join(output_file)), fps=10, verbose=False, progress_bar=False)


if __name__ == '__main__':
    make_gif(sys.argv[1], sys.argv[2])