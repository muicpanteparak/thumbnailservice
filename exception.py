class ObjectNotFoundException(LookupError):
    pass


class BucketCreationExcetion(LookupError):
    pass

class TicketCreationException(LookupError):
    pass

class ObjectCreationException(LookupError):
    pass