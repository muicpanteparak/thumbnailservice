FROM python:3.7-alpine
RUN mkdir -p /app/video
RUN apk --update add --no-cache file imagemagick build-base jpeg-dev zlib-dev
ENV LIBRARY_PATH=/lib:/usr/lib
VOLUME /app/video
WORKDIR /app
ADD requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN python -c 'import imageio; imageio.plugins.ffmpeg.download();'
ADD https://raw.githubusercontent.com/eficode/wait-for/master/wait-for .
RUN chmod +x wait-for && ls -la
ADD . .
RUN ls -la

CMD ["./wait-for", "rabbitmq:5672", "-t", "300", "--",  "python", "-u", "app.py"]
#RUN ln -s /app/make_thumbnail.py /bin/make_thumbnail
#RUN chmod +x /app/make_thumbnail.py